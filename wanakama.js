(function () { 
  var request = require('request');

  var Wanakama = function (api_key) {
    this.api_key = api_key;

    this.get = function (callback, endpoint, limit) {
      if (typeof limit === 'undefined') limit = '';
      request.get(this.URLS.URL_PREFIX + this.api_key + endpoint + limit, function (err, res, body) {
        callback(err, body);
      });
    };
  };

  Wanakama.prototype.URLS = {
    URL_PREFIX: "https://www.wanikani.com/api/user/",
    USER_INFORMATION: "/user-information/",
    STUDY_QUEUE: "/study-queue/",
    LEVEL_PROGRESSION: "/level-progression/",
    SRS_DISTRIBUTION: "/srs-distribution/",
    RECENT_UNLOCKS: "/recent-unlocks/",
    CRITICAL_ITEMS: "/critical-items/",
    RADICALS: "/radicals/",
    KANJI: "/kanji/",
    VOCABULARY: "/vocabulary/"
  };

  Wanakama.prototype.getUserInformation = function (callback) {
    this.get(callback, this.URLS.USER_INFORMATION);
  };

  Wanakama.prototype.getStudyQueue = function (callback) {
    this.get(callback, this.URLS.STUDY_QUEUE);
  };

  Wanakama.prototype.getLevelProgression = function (callback) {
    this.get(callback, this.URLS.LEVEL_PROGRESSION);
  };

  Wanakama.prototype.getSRSDistribution = function (callback) {
    this.get(callback, this.URLS.SRS_DISTRIBUTION);
  };

  Wanakama.prototype.getRecentUnlocks = function (callback, limit) {
    if (typeof limit === 'undefined') limit = 10;
    this.get(callback, this.URLS.RECENT_UNLOCKS, limit);
  };

  Wanakama.prototype.getCriticalItems = function (callback, limit) {
    if (typeof limit === 'undefined') limit = 75;
    this.get(callback, this.URLS.CRITICAL_ITEMS, limit);
  };

  Wanakama.prototype.getRadicals = function (callback, limit) {
    if (typeof limit === 'undefined') limit = '';
    else if (limit.isArray) limit = limit.join();

    this.get(callback, this.URLS.RADICALS, limit);
  };

  Wanakama.prototype.getKanji = function (callback, limit) {
    if (typeof limit === 'undefined') limit = '';
    else if (limit.isArray) limit = limit.join();

    this.get(callback, this.URLS.KANJI, limit);
  };

  Wanakama.prototype.getVocabulary = function (callback, limit) {
    if (typeof limit === 'undefined') limit = '';
    else if (limit.isArray) limit = limit.join();

    this.get(callback, this.URLS.VOCABULARY, limit);
  };

  module.exports = Wanakama;
})();