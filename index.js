var express = require('express'),
   Wanakama = require('./wanakama'),
          _ = require('underscore');

var app = express();
var wanakama = new Wanakama('ad9b33b546443798a15d3869258e66ff');

app.get('/', function (req, res) {
  wanakama.getKanji(function (err, body) {
    var result = JSON.parse(body);
    var kanjiList = result['requested_information'];

    var kanjiByLevels = _.groupBy(kanjiList, function (character) {
      return character["level"];
    });

    var minCharacters = _.map(kanjiByLevels, function (characters) {
      return _.min(characters, function (character) {
        return character["user_specific"]["unlocked_date"];
      });
    });

    var startTimes = _.map(minCharacters, function (character) {
      return new Date(character["user_specific"]["unlocked_date"] * 1000);
    });

    res.json(startTimes);
  });
});

app.listen(3000);